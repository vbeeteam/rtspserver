/**
******************************************************************************
* @file           : webe_logger.cpp
* @brief          : source file of Webe logger
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
/* Include -----------------------------------------------------------------*/
#include "logger/webe_logger.h"
#include "spdlog/sinks/rotating_file_sink.h"
#include "common/common_data.h"

WebeLogger::Logger spdLogger;
using namespace WebeLogger;

void Logger::init(const std::string &app_path){
    this->path = app_path +  std::string("/log/");
    std::string cmd = "mkdir -p " + this->path;
    system(cmd.c_str());    
    this->name = "common";
    this->common = spdlog::rotating_logger_mt(this->name, this->path + this->name + ".txt", 1048576 * 50, 20);
    //spdlog::set_default_logger(this->sysLogger);
    spdlog::flush_on(spdlog::level::info);
}