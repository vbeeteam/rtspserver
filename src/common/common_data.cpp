/**
******************************************************************************
* @file           : common_data.cpp
* @brief          : source file of common data
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#include "common/common_data.h"
#include "logger/webe_logger.h"
#include "utility/utility.hpp"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include <iostream>

using namespace BeexCommonData;
using namespace rapidjson;
using namespace CommonEngine;

RtspChannelHandle listRtspChannel;
MonitorStreamChannel listMonitorStream;

bool RtspChannelHandle::load(void){
    std::cout << "BoxConfig::loadConfig - start" << std::endl;

    bool result = false;
    // load file config
    if(UtilityFunc::checkFile(this->file_path)){
        FILE *file = NULL;
        size_t len;
        ssize_t read;
        char *line = NULL;
        file = fopen(this->file_path.c_str(), "r");
        if(file != NULL){
            read = getline(&line, &len, file);
            if(read != -1){
                std::string json_data(line);
                std::cout << "Config::loadConfig - read config file = " << json_data << std::endl;

                try{
                    // parse json data        
                    Document document;
                    document.Parse(json_data.c_str());

                    if(document.IsArray()){
                        for (size_t i = 0; i < document.Size(); i++){
                            const Value &channelInfo = document[i];
                            std::string cameraSerial = channelInfo["cameraSerial"].GetString();
                            std::string serviceId = channelInfo["serviceId"].GetString();
                            uint32_t port = channelInfo["port"].GetInt();
                            std::string host = channelInfo["host"].GetString();
                            std::string pubUsername = channelInfo["pubUsername"].GetString();
                            std::string pubPassword = channelInfo["pubPassword"].GetString();
                            std::string rtspStream = channelInfo["rtspStream"].GetString();
                            std::string name_space = channelInfo["name_space"].GetString();

                            RtspChannelInfo channel;
                            channel.cameraSerial = cameraSerial;
                            channel.serviceId = serviceId;
                            channel.port = port;
                            channel.pubUsername = pubUsername;
                            channel.pubPassword = pubPassword;
                            channel.rtspStream = rtspStream;
                            channel.name_space = name_space;
                            channel.isNewChannel = true;
                            channel.isActive = true;

                            this->addChannel(channel);
                        }
                        
                    }

                    result = true;
                }
                catch(const std::exception& e){
                    std::cout << "Config::loadConfig - exception when parse json data = " << json_data << std::endl;
                    result = false;
                }
            }
            else{
                std::cout << "Config::loadConfig - config file not read" << std::endl;
            }
        }
        else{
            std::cout << "Config::loadConfig - config file not opened" << std::endl;
        }
        fclose(file);
    }
    else{
        std::cout << "Config::loadConfig - config file not found" << std::endl;
        UtilityFunc::creatFile(this->file_path);
    }
    return result;
}


bool RtspChannelHandle::save(void){
    StringBuffer string_buffer;
    Writer<StringBuffer> writer(string_buffer);

    writer.StartArray();

    std::map<std::string, RtspChannelInfo>::iterator map;
    for (map = this->listChannel.begin(); map != this->listChannel.end(); map++){
        RtspChannelInfo channel = map->second;
        writer.StartObject();
        writer.Key("cameraSerial");
        writer.String(channel.cameraSerial.c_str());
        writer.Key("serviceId");
        writer.String(channel.serviceId.c_str());
        writer.Key("port");
        writer.Int(channel.port);
        writer.Key("host");
        writer.String(channel.host.c_str());
        writer.Key("pubUsername");
        writer.String(channel.pubUsername.c_str());
        writer.Key("pubPassword");
        writer.String(channel.pubPassword.c_str());
        writer.Key("rtspStream");
        writer.String(channel.rtspStream.c_str());
        writer.Key("name_space");
        writer.String(channel.name_space.c_str());
        writer.EndObject();
    }

    writer.EndArray();

    std::string data = string_buffer.GetString();

    //std::cout << out.str() << std::endl;

    // save to file
    bool result = false;
    std::string configFile = this->file_path;
    FILE *file = NULL;

    file = fopen(configFile.c_str(), "w");
    if(file != NULL){
        fputs(data.c_str(), file);
        result = true;
    }
    fclose(file);

    return result;
}

bool RtspChannelHandle::addChannel(RtspChannelInfo &channelInfo){
    bool result = false;
    std::string key = channelInfo.cameraSerial + ":" + channelInfo.serviceId;
    this->locker.lock();
    if(this->listChannel.find(key) == this->listChannel.end()){
        this->listChannel[key] = channelInfo;
        result = true;
        this->save();
    }
    else{
        spdLogger.common->error("RtspChannelHandle::addChannel - exited channelKey = {0}", key);
    }
    this->locker.unlock();

    return result;
}

bool RtspChannelHandle::findChannel(RtspChannelInfo &channelInfo){
    bool result = false;
    std::string key = channelInfo.cameraSerial + ":" + channelInfo.serviceId;
    this->locker.lock();
    if(this->listChannel.find(key) != this->listChannel.end()){
        result = true;
    }
    else{
        spdLogger.common->error("RtspChannelHandle::findChannel - not found channelKey = {0}", key);
    }
    this->locker.unlock();

    return result;
}

bool RtspChannelHandle::getChannel(std::string channelKey, RtspChannelInfo *channelInfo){
    bool result = false;
    this->locker.lock();
    if(this->listChannel.find(channelKey) != this->listChannel.end()){
        *channelInfo = this->listChannel[channelKey];
        result = true;
    }
    else{
        spdLogger.common->error("RtspChannelHandle::getChannel - not found channelKey = {0}", channelKey);
    }
    this->locker.unlock();

    return result;
}

bool RtspChannelHandle::getNewChannel(RtspChannelInfo *channelInfo){
    bool result = false;
    this->locker.lock();
    std::map<std::string, RtspChannelInfo>::iterator map;
    for(map = this->listChannel.begin(); map != this->listChannel.end(); map++){
        RtspChannelInfo channel;
        channel = map->second;
        if(channel.isNewChannel){
            *channelInfo = channel;
            result = true;
            break;
        }
    }
    this->locker.unlock();

    return result;
}


bool RtspChannelHandle::eraseChannel(RtspChannelInfo &channelInfo){
    bool result = false;
    this->locker.lock();
    std::string channelKey = channelInfo.cameraSerial + ":" + channelInfo.serviceId;
    if(this->listChannel.find(channelKey) != this->listChannel.end()){
        this->listChannel.erase(channelKey);
        result = true;
        this->save();
    }
    else{
        spdLogger.common->error("RtspChannelHandle::eraseChannel - not found channelKey = {0}", channelKey);
    }
    this->locker.unlock();
    return result;
}



bool RtspChannelHandle::findPortChannel(uint32_t port){
    bool result = false;
    this->locker.lock();
    std::map<std::string, RtspChannelInfo>::iterator map;
    for(map = this->listChannel.begin(); map != this->listChannel.end(); map++){
        RtspChannelInfo channel;
        channel = map->second;
        if(channel.port == port){
            result = true;
            break;
        }
    }
    this->locker.unlock();

    return result;
}




bool RtspChannelHandle::updateActiveState(RtspChannelInfo &channelInfo, bool &state){
    bool result = false;
    this->locker.lock();

    std::string key = channelInfo.cameraSerial + ":" + channelInfo.serviceId;
    if(this->listChannel.find(key) != this->listChannel.end()){
        RtspChannelInfo channelUpdate;
        channelUpdate = this->listChannel[key];
        channelUpdate.isActive = state;
        this->listChannel[key] = channelUpdate;
        result = true;
    }
    else{
        spdLogger.common->error("RtspChannelHandle::updateActiveState - not found channelKey = {0}", key);
    }
    this->locker.unlock();

    return result;
}


bool RtspChannelHandle::updateRunState(RtspChannelInfo &channelInfo, bool &state){
    bool result = false;
    this->locker.lock();

    std::string key = channelInfo.cameraSerial + ":" + channelInfo.serviceId;
    if(this->listChannel.find(key) != this->listChannel.end()){
        RtspChannelInfo channelUpdate;
        channelUpdate = this->listChannel[key];
        channelUpdate.isRunning = state;
        this->listChannel[key] = channelUpdate;
        result = true;
    }
    else{
        spdLogger.common->error("RtspChannelHandle::updateRunState - not found channelKey = {0}", key);
    }
    this->locker.unlock();

    return result;
}


bool RtspChannelHandle::updateSubProcessId(RtspChannelInfo &channelInfo, uint32_t &p_id){
    bool result = false;
    this->locker.lock();

    std::string key = channelInfo.cameraSerial + ":" + channelInfo.serviceId;
    if(this->listChannel.find(key) != this->listChannel.end()){
        RtspChannelInfo channelUpdate;
        channelUpdate = this->listChannel[key];
        channelUpdate.subprocessId = p_id;
        this->listChannel[key] = channelUpdate;
        result = true;
    }
    else{
        spdLogger.common->error("RtspChannelHandle::updateSubProcessId - not found channelKey = {0}", key);
    }
    this->locker.unlock();

    return result;
}


bool RtspChannelHandle::clearNewChannelState(RtspChannelInfo &channelInfo){
    bool result = false;
    this->locker.lock();

    std::string key = channelInfo.cameraSerial + ":" + channelInfo.serviceId;
    if(this->listChannel.find(key) != this->listChannel.end()){
        RtspChannelInfo channelUpdate;
        channelUpdate = this->listChannel[key];
        channelUpdate.isNewChannel = false;
        this->listChannel[key] = channelUpdate;
        result = true;
    }
    else{
        spdLogger.common->error("RtspChannelHandle::clearNewChannelState - not found channelKey = {0}", key);
    }
    this->locker.unlock();    

    return result;
}


bool MonitorStreamChannel::load(void){
    std::cout << "BoxConfig::loadConfig - start" << std::endl;

    bool result = false;
    // load file config
    if(UtilityFunc::checkFile(this->file_path)){
        FILE *file = NULL;
        size_t len;
        ssize_t read;
        char *line = NULL;
        file = fopen(this->file_path.c_str(), "r");
        if(file != NULL){
            read = getline(&line, &len, file);
            if(read != -1){
                std::string json_data(line);
                std::cout << "Config::loadConfig - read config file = " << json_data << std::endl;

                try{
                    // parse json data        
                    Document document;
                    document.Parse(json_data.c_str());

                    if(document.IsArray()){
                        for (size_t i = 0; i < document.Size(); i++){
                            const Value &channelInfo = document[i];
                            std::string serial = channelInfo["serial"].GetString();
                            uint16_t monitorStreamId = channelInfo["monitorStreamId"].GetInt();
                            uint16_t serviceId = channelInfo["serviceId"].GetInt();
                            uint64_t port = channelInfo["port"].GetInt();
                            std::string host = channelInfo["host"].GetString();
                            std::string username = channelInfo["username"].GetString();
                            std::string password = channelInfo["password"].GetString();
                            std::string name_space = channelInfo["name_space"].GetString();
                            std::string streamPath = channelInfo["streamPath"].GetString();
                            std::string scheme = channelInfo["scheme"].GetString();

                            MonitorStreamInfo channel;
                            channel.serial = serial;
                            channel.service_id = serviceId;
                            channel.monitor_stream_id = monitorStreamId;
                            channel.port = port;
                            channel.host = host;
                            channel.username = username;
                            channel.password = password;
                            channel.name_space = name_space;
                            channel.stream_path = streamPath;
                            channel.scheme = scheme;

                            this->addChannel(channel);
                        }
                        
                    }

                    result = true;
                }
                catch(const std::exception& e){
                    std::cout << "Config::loadConfig - exception when parse json data = " << json_data << std::endl;
                    result = false;
                }
            }
            else{
                std::cout << "Config::loadConfig - config file not read" << std::endl;
            }
        }
        else{
            std::cout << "Config::loadConfig - config file not opened" << std::endl;
        }
        fclose(file);
    }
    else{
        std::cout << "Config::loadConfig - config file not found" << std::endl;
        UtilityFunc::creatFile(this->file_path);
    }
    return result;
}


bool MonitorStreamChannel::save(void){
    StringBuffer string_buffer;
    Writer<StringBuffer> writer(string_buffer);

    writer.StartArray();

    std::map<std::string, MonitorStreamInfo>::iterator map;
    for (map = this->listChannel.begin(); map != this->listChannel.end(); map++){
        MonitorStreamInfo channel = map->second;
        writer.StartObject();
        writer.Key("serial");
        writer.String(channel.serial.c_str());
        writer.Key("serviceId");
        writer.Int(channel.service_id);
        writer.Key("monitorStreamId");
        writer.Int(channel.monitor_stream_id);
        writer.Key("port");
        writer.Int(channel.port);
        writer.Key("host");
        writer.String(channel.host.c_str());
        writer.Key("username");
        writer.String(channel.username.c_str());
        writer.Key("password");
        writer.String(channel.password.c_str());
        writer.Key("streamPath");
        writer.String(channel.stream_path.c_str());
        writer.Key("namespace");
        writer.String(channel.name_space.c_str());
        writer.Key("scheme");
        writer.String(channel.scheme.c_str());
        writer.EndObject();
    }

    writer.EndArray();

    std::string data = string_buffer.GetString();

    //std::cout << out.str() << std::endl;

    // save to file
    bool result = false;
    std::string configFile = this->file_path;
    FILE *file = NULL;

    file = fopen(configFile.c_str(), "w");
    if(file != NULL){
        fputs(data.c_str(), file);
        result = true;
    }
    fclose(file);

    return result;
}

bool MonitorStreamChannel::addChannel(const MonitorStreamInfo &channelInfo){
    bool result = false;
    std::string key;
    this->createKey(channelInfo, key);
    this->locker.lock();
    if(this->listChannel.find(key) == this->listChannel.end()){
        this->listChannel[key] = channelInfo;
        result = true;
        this->save();
    }
    else{
        spdLogger.common->error("MonitorStreamChannel::addChannel - exited channelKey = {0}", key);
    }
    this->locker.unlock();

    return result;
}

bool MonitorStreamChannel::findChannel(const MonitorStreamInfo &channelInfo){
    bool result = false;
    std::string key;
    this->createKey(channelInfo, key);
    this->locker.lock();
    if(this->listChannel.find(key) != this->listChannel.end()){
        result = true;
    }
    else{
        spdLogger.common->error("MonitorStreamChannel::findChannel - not found channelKey = {0}", key);
    }
    this->locker.unlock();

    return result;
}

bool MonitorStreamChannel::getChannel(std::string channelKey, MonitorStreamInfo &channelInfo){
    bool result = false;
    this->locker.lock();
    if(this->listChannel.find(channelKey) != this->listChannel.end()){
        channelInfo = this->listChannel[channelKey];
        result = true;
    }
    else{
        spdLogger.common->error("MonitorStreamChannel::getChannel - not found channelKey = {0}", channelKey);
    }
    this->locker.unlock();

    return result;
}


bool MonitorStreamChannel::eraseChannel(const MonitorStreamInfo &channelInfo){
    bool result = false;
    this->locker.lock();
    std::string key;
    this->createKey(channelInfo, key);
    if(this->listChannel.find(key) != this->listChannel.end()){
        this->listChannel.erase(key);
        result = true;
        this->save();
    }
    else{
        spdLogger.common->error("RtspChannelHandle::eraseChannel - not found channelKey = {0}", key);
    }
    this->locker.unlock();
    return result;
}
