/**
******************************************************************************
* @file           : rtsp_server.cpp
* @brief          : source file of rtsp server
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#include "rtspserver/rtsp_server.h"
#include "logger/webe_logger.h"
#include "subprocess/subprocess.hpp"
#include <unistd.h>
#include "api_service/api_httpclient.h"

using namespace BeexCommonData;
using namespace BeexRtspServer;
using namespace subprocess;
using namespace BeexApiService;

RtspServer workerRtspServer;

void RtspServer::startChannel(RtspChannelInfo channelInfo){
    try
    {
        std::string channelKey = channelInfo.cameraSerial + ":" + channelInfo.serviceId;
        RtspChannelInfo mychannel;
        if(listRtspChannel.getChannel(channelKey, &mychannel)){
            bool state = true;
            listRtspChannel.updateRunState(mychannel, state);
            spdLogger.common->info("RtspServer::startChannel - channelKey = {0}", channelKey);
            //  + " " + 
            std::string cmd = BEEX_PATH + std::string("rtsp-simple-server ") + BEEX_PATH + "rtsp-simple-server.yml";
            auto p = Popen({cmd}, shell{false}, environment{{
                        {"RTSP_RTSPPORT", std::to_string(mychannel.port)}
                                        }});
            usleep(300000);
            
            uint32_t p_id = p.pid();
            listRtspChannel.updateSubProcessId(mychannel, p_id);
            spdLogger.common->info("RtspServer::startChannel - running, channelKey = {0}, pid = {1}", channelKey, std::to_string(p_id));
            while (1){
                if(p.poll() != -1){
                    spdLogger.common->warn("RtspServer::startChannel - crash, channelKey = {0}, pid = {1}, retcode = {2}", channelKey, std::to_string(p_id), std::to_string(p.retcode()));
                    state = false;
                    listRtspChannel.updateRunState(mychannel, state);
                    break;
                }
                usleep(100000);
            }
            spdLogger.common->info("RtspServer::startChannel - end channelKey = {0}", channelKey);
        }
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
}


bool RtspServer::stopChannel(RtspChannelInfo channelInfo){
    std::string channelKey = channelInfo.cameraSerial + ":" + channelInfo.serviceId;
    spdLogger.common->info("RtspServer::stopChannel - kill channelKey = {0}, pid = {1}", channelKey, std::to_string(channelInfo.subprocessId));
    kill(channelInfo.subprocessId, SIGKILL);
    return true;
}


void RtspServer::handleChannel(RtspChannelInfo channelInfo){
    std::string channelKey = channelInfo.cameraSerial + ":" + channelInfo.serviceId;
    spdLogger.common->info("RtspServer::handleChannel - create handle channel thread for channelKey = {0}", channelKey);
    this->channelNum++;
    ApiHttpClient reportApi;
    bool reportResult = false;
    uint32_t timeReport = 40;
    
    while (1){
        RtspChannelInfo mychannel; 
        if(listRtspChannel.getChannel(channelKey, &mychannel)){
            if(mychannel.isActive == true){
                if(mychannel.isRunning == false){
                    std::thread RunChannelThread(&RtspServer::startChannel, this, mychannel);
                    RunChannelThread.detach();
                    sleep(2);
                }
            }
            else{
                spdLogger.common->info("RtspServer::handleChannel - Inactive and erase channel for channelKey = {0}", channelKey);
                this->stopChannel(mychannel);
                sleep(1);
                reportApi.deleteRtspChannelStream(mychannel.cameraSerial, mychannel.serviceId);
                listRtspChannel.eraseChannel(mychannel);
                break;
            }
            timeReport++;
            if(timeReport > 30){
                timeReport = 0;
                if(!reportResult){
                    if(reportApi.addRtspChannelStream(mychannel.cameraSerial, mychannel.serviceId, mychannel.rtspStream, mychannel.name_space)){
                        reportResult = true;
                    }
                }
            }
        }
        usleep(100000);
    }
    this->channelNum--;
    // spdLogger.common->info("RtspServer::handleChannel - stop handle channel thread for channelKey = {0}", channelKey);
}


void RtspServer::run(void){
    spdLogger.common->info("RtspServer::run - started");
    uint32_t time_debug = 0;
    while (1){
        RtspChannelInfo channelInfo;
        if(listRtspChannel.getNewChannel(&channelInfo)){
            std::thread ChannelHandleThread(&RtspServer::handleChannel, this, channelInfo);
            ChannelHandleThread.detach();
            listRtspChannel.clearNewChannelState(channelInfo);
        }
        usleep(10000);
        time_debug++;
        if(!(time_debug % 100)){
            spdLogger.common->info("RtspServer - channel number = {0}", std::to_string(this->channelNum));
        }
    }
    spdLogger.common->info("RtspServer::run - stopped");
}