/**
******************************************************************************
* @file           : api_httpclient.cpp
* @brief          : source file of API HttpClient
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#include "api_service/api_httpclient.h"
#include "logger/webe_logger.h"
#include <Poco/Net/HTTPClientSession.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTMLForm.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/JSON/Object.h>
#include <Poco/JSON/Parser.h>
#include <Poco/JSON/Array.h>
#include <Poco/URI.h>
#include <Poco/StreamCopier.h>
#include <Poco/Dynamic/Var.h>

#include "Poco/SharedPtr.h"
#include "Poco/Net/KeyConsoleHandler.h"
#include "Poco/Net/ConsoleCertificateHandler.h"
#include "Poco/Net/HTTPSClientSession.h"
#include "Poco/Net/AcceptCertificateHandler.h"

using namespace BeexApiService;
using namespace Poco::Net;
using namespace Poco;


bool ApiHttpClient::requestPostByHttps(std::string &api, std::string &data, std::string *msgResp){
    try{
        Poco::URI uri(api);
        std::string path(uri.getPathAndQuery());
        if(path.empty()){
            path = "/";
        }
        SSLInitializer sslInitializer;
    
        Poco::SharedPtr<Poco::Net::InvalidCertificateHandler> ptrCert = new Poco::Net::AcceptCertificateHandler(false);
        Poco::Net::Context::Ptr ptrContext = new Poco::Net::Context(Poco::Net::Context::CLIENT_USE, "", "", this->cacertPath + std::string("cacert.pem"),  Poco::Net::Context::VERIFY_NONE);
        Poco::Net::SSLManager::instance().initializeClient(0, ptrCert, ptrContext);

        Poco::Net::HTTPSClientSession session(uri.getHost(), uri.getPort());
        Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, path, Poco::Net::HTTPMessage::HTTP_1_1);
        Poco::Net::HTTPResponse response;
        std::ostringstream message;

        request.setContentType("application/json");
        request.setContentLength(data.length());
        request.add("Authorization", "aNzKIUuTzkILETuFpVxidmudRzsWG2YS");

        std::ostream &os = session.sendRequest(request);
        os << data;
        std::istream &rsp = session.receiveResponse(response);
        std::stringstream messgae;
        Poco::StreamCopier::copyStream(rsp, message);

        if(response.getStatus() == Poco::Net::HTTPResponse::HTTP_OK){
            std::cout << "ApiHttpClient::requestPostByHttps - response : " << message.str() << std::endl;
            *msgResp = message.str();
            return true;
        }

    }
    catch(const std::exception& e){
        std::cerr << e.what() << '\n';
    }
    return false;          
}


bool ApiHttpClient::requestPostByHttps(std::string &api, std::string &token, std::string &data, std::string *msgResp){
    try{
        Poco::URI uri(api);
        std::string path(uri.getPathAndQuery());
        if(path.empty()){
            path = "/";
        }
        SSLInitializer sslInitializer;
    
        Poco::SharedPtr<Poco::Net::InvalidCertificateHandler> ptrCert = new Poco::Net::AcceptCertificateHandler(false);
        Poco::Net::Context::Ptr ptrContext = new Poco::Net::Context(Poco::Net::Context::CLIENT_USE, "", "", this->cacertPath + std::string("cacert.pem"),  Poco::Net::Context::VERIFY_NONE);
        Poco::Net::SSLManager::instance().initializeClient(0, ptrCert, ptrContext);

        Poco::Net::HTTPSClientSession session(uri.getHost(), uri.getPort());
        Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, path, Poco::Net::HTTPMessage::HTTP_1_1);
        Poco::Net::HTTPResponse response;
        std::ostringstream message;

        request.setContentType("application/json");
        request.setContentLength(data.length());
        request.add("Authorization", token);

        std::ostream &os = session.sendRequest(request);
        os << data;
        std::istream &rsp = session.receiveResponse(response);
        std::stringstream messgae;
        Poco::StreamCopier::copyStream(rsp, message);

        if(response.getStatus() == Poco::Net::HTTPResponse::HTTP_OK){
            std::cout << "ApiHttpClient::requestPostByHttps - response : " << message.str() << std::endl;
            *msgResp = message.str();
            return true;
        }

    }
    catch(const std::exception& e){
        std::cerr << e.what() << '\n';
    }
    return false;          
}

bool ApiHttpClient::requestPostByHttp(std::string &api, std::string &data, std::string *msgResp){
    try{
        Poco::URI uri(api);
        std::string path(uri.getPathAndQuery());
        if(path.empty()){
            path = "/";
        }
        Poco::Net::HTTPClientSession session(uri.getHost(), uri.getPort());
        Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, path, Poco::Net::HTTPMessage::HTTP_1_1);
        Poco::Net::HTTPResponse response;
        std::ostringstream message;

        request.setContentType("application/json");
        request.setContentLength(data.length());
        request.add("Authorization", "aNzKIUuTzkILETuFpVxidmudRzsWG2YS");

        std::ostream &os = session.sendRequest(request);
        os << data;        

        std::istream &rsp = session.receiveResponse(response);
        Poco::StreamCopier::copyStream(rsp, message);

        if(response.getStatus() == Poco::Net::HTTPResponse::HTTP_OK){
            // std::cout << "ApiHttpClient::requestDeleteByHttp - response : " << message.str() << std::endl;
            spdLogger.common->info("ApiHttpClient::requestDeleteByHttp - response : = {0}", message.str());
            *msgResp = message.str();
            return true;
        }
        else{
            spdLogger.common->error("ApiHttpClient::requestDeleteByHttp - status error - respone : = {0}", message.str());
        }

    }
    catch(const std::exception& e){
        std::cerr << e.what() << '\n';
    }
    return false;
}

bool ApiHttpClient::requestDeleteByHttps(std::string &api, std::string &data, std::string *msgResp){
    try{
        Poco::URI uri(api);
        std::string path(uri.getPathAndQuery());
        if(path.empty()){
            path = "/";
        }
        SSLInitializer sslInitializer;
    
        Poco::SharedPtr<Poco::Net::InvalidCertificateHandler> ptrCert = new Poco::Net::AcceptCertificateHandler(false);
        Poco::Net::Context::Ptr ptrContext = new Poco::Net::Context(Poco::Net::Context::CLIENT_USE, "", "", this->cacertPath + std::string("cacert.pem"),  Poco::Net::Context::VERIFY_NONE);
        Poco::Net::SSLManager::instance().initializeClient(0, ptrCert, ptrContext);

        Poco::Net::HTTPSClientSession session(uri.getHost(), uri.getPort());
        Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_DELETE, path, Poco::Net::HTTPMessage::HTTP_1_1);
        Poco::Net::HTTPResponse response;
        std::ostringstream message;

        request.setContentType("application/json");
        request.setContentLength(data.length());
        request.add("Authorization", "aNzKIUuTzkILETuFpVxidmudRzsWG2YS");

        std::ostream &os = session.sendRequest(request);
        os << data;
        std::istream &rsp = session.receiveResponse(response);
        std::stringstream messgae;
        Poco::StreamCopier::copyStream(rsp, message);

        if(response.getStatus() == Poco::Net::HTTPResponse::HTTP_OK){
            std::cout << "ApiHttpClient::requestDeleteByHttps - response : " << message.str() << std::endl;
            *msgResp = message.str();
            return true;
        }

    }
    catch(const std::exception& e){
        std::cerr << e.what() << '\n';
    }
    return false;          
}


bool ApiHttpClient::requestDeleteByHttp(std::string &api, std::string &data, std::string *msgResp){
    try{
        Poco::URI uri(api);
        std::string path(uri.getPathAndQuery());
        if(path.empty()){
            path = "/";
        }
        Poco::Net::HTTPClientSession session(uri.getHost(), uri.getPort());
        Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_DELETE, path, Poco::Net::HTTPMessage::HTTP_1_1);
        Poco::Net::HTTPResponse response;
        std::ostringstream message;

        request.setContentType("application/json");
        request.setContentLength(data.length());
        request.add("Authorization", "aNzKIUuTzkILETuFpVxidmudRzsWG2YS");

        std::ostream &os = session.sendRequest(request);
        os << data;        

        std::istream &rsp = session.receiveResponse(response);
        Poco::StreamCopier::copyStream(rsp, message);

        if(response.getStatus() == Poco::Net::HTTPResponse::HTTP_OK){
            // std::cout << "ApiHttpClient::requestDeleteByHttp - response : " << message.str() << std::endl;
            spdLogger.common->info("ApiHttpClient::requestDeleteByHttp - response : = {0}", message.str());
            *msgResp = message.str();
            return true;
        }
        else{
            spdLogger.common->error("ApiHttpClient::requestDeleteByHttp - status error - respone : = {0}", message.str());
        }

    }
    catch(const std::exception& e){
        std::cerr << e.what() << '\n';
    }
    return false;
}


bool ApiHttpClient::addRtspChannelStream(std::string &cameraSerial, std::string &serviceId, std::string &streamUrl, std::string &name_space){
    try
    {
        std::string api = "http://103.63.109.73:9099/stream";
        JSON::Object object;
        object.set("deviceSerial", cameraSerial);
        object.set("streamUrl", streamUrl);
        object.set("serviceId", std::stoi(serviceId));
        object.set("namespace", name_space);

        std::stringstream out;
        object.stringify(out);

        std::string jsonData = out.str();
        std::string msgResp = "";
        spdLogger.common->info("ApiHttpClient::addRtspChannelStream - rawData = {0}", jsonData);

        if(this->requestPostByHttp(api, jsonData, &msgResp)){
            
        }

    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
    
}

bool ApiHttpClient::deleteRtspChannelStream(std::string &cameraSerial, std::string &serviceId){
    try
    {
        std::string api = "http://103.63.109.73:9099/stream";
        JSON::Object object;
        object.set("deviceSerial", cameraSerial);
        object.set("serviceId", std::stoi(serviceId));

        std::stringstream out;
        object.stringify(out);

        std::string jsonData = out.str();
        std::string msgResp = "";
        spdLogger.common->info("ApiHttpClient::deleteRtspChannelStream - rawData = {0}", jsonData);

        if(this->requestDeleteByHttp(api, jsonData, &msgResp)){
            
        }

    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
    
}