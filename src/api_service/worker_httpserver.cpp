/**
******************************************************************************
* @file           : worker_httpserver.cpp
* @brief          : Source file of Worker for http server
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
/* Include ------------------------------------------------------------------*/
#include "api_service/worker_httpserver.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/HTTPServer.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Util/ServerApplication.h"
#include "Poco/JSON/Object.h"
#include "Poco/URI.h"
#include <iostream>
#include <string>
#include <vector>

#include "common/common_data.h"
#include "resources/manage_port_listen.h"

using namespace Poco::Net;
using namespace Poco::Util;
using namespace Poco::JSON;
using namespace Poco;
using namespace BeexApiService;
using namespace BeexCommonData;

#define LOCAL_IP            "103.63.108.20"


class MyRequestHandler : public HTTPRequestHandler{
public:
    virtual void handleRequest(HTTPServerRequest &req, HTTPServerResponse &resp){
        resp.setStatus(HTTPResponse::HTTP_OK);
        resp.setContentType("application/json");

        std::cout << "WorkerHttpServer - get Http Request " << req.getURI() << std::endl;

        std::string response;

        URI uri(req.getURI());
        std::string pathName = uri.getPath();

        if(req.getMethod() == "GET"){
            if(pathName == "/getRtsp"){
                // http://127.0.0.1:8080/control?serial=12341234&host=192.168.0.105&port=10006
                URI::QueryParameters param = uri.getQueryParameters();
                std::pair<std::string, std::string> paramSerial = param[0];
                std::pair<std::string, std::string> paramHost = param[1];
                std::pair<std::string, std::string> paramPort = param[2];
                std::pair<std::string, std::string> paramServiceId = param[3];
                std::pair<std::string, std::string> paramNamespace = param[4];

                std::string keySerial = paramSerial.first;
                std::string keyHost = paramHost.first;
                std::string keyPort = paramPort.first;
                std::string keyServiceId = paramServiceId.first;
                std::string keyNamespace = paramNamespace.first;

                if((keySerial == "serial") && (keyHost == "host") && (keyPort == "port") && (keyServiceId == "serviceId") && (keyNamespace == "namespace")){
                    std::string serial = paramSerial.second;
                    std::string rtp_host = paramHost.second;
                    std::string rtp_port = paramPort.second;
                    std::string serviceId = paramServiceId.second;
                    std::string name_space = paramNamespace.second;
                    std::string keychannel = serial + ":" + serviceId;

                    RtspChannelInfo chanelInfo;
                    chanelInfo.cameraSerial = serial;
                    chanelInfo.serviceId = serviceId;
                    chanelInfo.name_space = name_space;
                    chanelInfo.isNewChannel = true;
                    if(listRtspChannel.findChannel(chanelInfo)){
                        std::string keyChannel = chanelInfo.cameraSerial + ":" + chanelInfo.serviceId;
                        listRtspChannel.getChannel(keychannel, &chanelInfo);
                    }
                    else{
                        // get port
                        uint32_t portAvailable = workerManagePortListen.getPortAvailable();
                        if(portAvailable > 0){
                            chanelInfo.port = portAvailable;
                            chanelInfo.isNewChannel = true;
                            chanelInfo.isActive = true;
                            chanelInfo.rtspStream = "rtsp://" + std::string(LOCAL_IP) + ":" + std::to_string(chanelInfo.port) + "/mystream";
                            listRtspChannel.addChannel(chanelInfo);
                        }
                    }
                    Object object;
                    object.set("serial", serial);
                    object.set("rtspHost", LOCAL_IP);
                    object.set("rtspPort", chanelInfo.port);
                    std::stringstream out_str;
                    object.stringify(out_str);
                    response = out_str.str();
                    std::ostream& out = resp.send();
                    out << response;
                    out.flush();
                    std::cout << " WorkerHttpServer - HttpServer Response: " << response << std::endl;                    
                    }
            }
            else if(pathName == "/deleteRtsp"){
                // http://127.0.0.1:8080/control?serial=12341234&host=192.168.0.105&port=10006
                URI::QueryParameters param = uri.getQueryParameters();
                std::pair<std::string, std::string> paramSerial = param[0];
                std::pair<std::string, std::string> paramHost = param[1];
                std::pair<std::string, std::string> paramPort = param[2];

                std::string keySerial = paramSerial.first;
                std::string keyHost = paramHost.first;
                std::string keyPort = paramPort.first;

                if((keySerial == "serial") && (keyHost == "host") && (keyPort == "port")){
                    std::string serial = paramSerial.second;
                    std::string rtp_host = paramHost.second;
                    std::string rtp_port = paramPort.second;
                    std::string serviceId = "4";
                    std::string keychannel = serial + ":" + serviceId;

                    RtspChannelInfo chanelInfo;
                    chanelInfo.cameraSerial = serial;
                    chanelInfo.serviceId = serviceId;
                    chanelInfo.isNewChannel = true;
                    if(listRtspChannel.findChannel(chanelInfo)){
                        std::string keyChannel = chanelInfo.cameraSerial + ":" + chanelInfo.serviceId;
                        listRtspChannel.getChannel(keychannel, &chanelInfo);
                        bool state = false;
                        listRtspChannel.updateActiveState(chanelInfo, state);
                    }
                    std::ostream& out = resp.send();
                    out.flush();
                    std::cout << " WorkerHttpServer - HttpServer Response: " << std::endl;                    
                    }
            }
        }
    }

private:
  static int count;
};

int MyRequestHandler::count = 0;

class MyRequestHandlerFactory : public HTTPRequestHandlerFactory{
public:
    virtual HTTPRequestHandler* createRequestHandler(const HTTPServerRequest &)
    {
        return new MyRequestHandler;
    }
};

class MyServerApp : public ServerApplication{
    private:
        uint32_t portListen;
protected:
    int main(const std::vector<std::string> &){
        this->portListen = 40000;
        HTTPServer s(new MyRequestHandlerFactory, ServerSocket(this->portListen), new HTTPServerParams);

        s.start();
        std::cout << "HttpServer started" << std::endl;

        waitForTerminationRequest();  // wait for CTRL-C or kill

        std::cout << "HttpServer Stopped" << std::endl;
        s.stop();

        return Application::EXIT_OK;
    }
};


void WorkerHttpServer::run(void){
    MyServerApp app;
    int argc = 1;
    char* argv = "ABC";
    app.run(argc, &argv);
}