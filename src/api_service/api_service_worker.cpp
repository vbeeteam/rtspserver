/**
******************************************************************************
* @file           : api_service_worker.cpp
* @brief          : Header file of Worker for api service
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
/*Include -------------------------------------------------------------------*/
#include "api_service/api_service_worker.hpp"
#include "logger/webe_logger.h"
#include "config/config.hpp"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "common/common_data.h"

using namespace ApiService;
using namespace httplib;
using namespace rapidjson;
using namespace BeexCommonData;

ApiServiceWorker apiServiceWorker;

void ApiServiceWorker::init(void){
    this->addMonitorStreamChannel();
}

void ApiServiceWorker::addMonitorStreamChannel(void){
    this->svr.Post("/beex/media/add_monitor_stream", [&](const Request &req, Response &res, const ContentReader &content_reader){
        std::string body;
        std::string body_res = "{\"result\":0}";
        content_reader([&](const char *data, size_t data_length){
            body.append(data, data_length);
            spdLogger.common->info("ApiServiceWorker::addMonitorStreamChannel - body = {}", body);
            // processing
            Document document;
            document.Parse(body.c_str());
            if(document.HasMember("serial") && document.HasMember("serviceId") && document.HasMember("namespace") && document.HasMember("monitorStreamId")){
                MonitorStreamInfo channel;

                std::string serial = document["serial"].GetString();
                uint16_t monitorStreamId = document["monitorStreamId"].GetInt();
                uint16_t serviceId = document["serviceId"].GetInt();
                std::string name_space = document["namespace"].GetString();

                if(document.HasMember("gatewaySerial")){
                    std::string gateway_serial = document["gatewaySerial"].GetString();
                    channel.gateway_serial = gateway_serial;
                }

                // get config
                CommonEngine::config_info common_info;
                commonConfig.getConfig(common_info);

                channel.serial = serial;
                channel.service_id = serviceId;
                channel.monitor_stream_id = monitorStreamId;
                channel.port = common_info.rtsp_monitor_stream_port;
                channel.host = common_info.public_ip;
                channel.username = common_info.rtsp_username;
                channel.password = common_info.rtsp_password;
                channel.name_space = name_space;
                channel.scheme = "rtsp";

                std::string key;
                listMonitorStream.createKey(channel, key);
                channel.stream_path = key;
                listMonitorStream.addChannel(channel);

                // respone
                StringBuffer json_buffer;
                Writer<StringBuffer> writer(json_buffer);

                writer.StartObject();
                writer.Key("serial");
                writer.String(channel.serial.c_str());
                writer.Key("username");
                writer.String(channel.username.c_str());
                writer.Key("password");
                writer.String(channel.password.c_str());
                writer.Key("host");
                writer.String(channel.host.c_str());
                writer.Key("port");
                writer.Int(channel.port);
                writer.Key("streamPath");
                writer.String(channel.stream_path.c_str());
                writer.EndObject();

                body_res = json_buffer.GetString();
            }
            return true;
        });
        spdLogger.common->info("ApiServiceWorker::addMonitorStreamChannel - response = {}", body_res);
        res.set_content(body_res, "application/json");
    });
}

void ApiServiceWorker::run(void){
    spdLogger.common->info("ApiServiceWorker - start");
    this->init();

    CommonEngine::config_info common_info;
    commonConfig.getConfig(common_info);

    // start api server
    if(common_info.api_service_port > 0){
        this->port = common_info.api_service_port;
    }

    spdLogger.common->info("ApiServiceWorker - listen as ip = {}, port = {}", this->ip_addr, this->port);
    this->svr.listen(this->ip_addr.c_str(), this->port);
    spdLogger.common->info("ApiServiceWorker - stop");
}