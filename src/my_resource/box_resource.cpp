/**
******************************************************************************
* @file           : box_resource.cpp
* @brief          : source file of resource in box
******************************************************************************
******************************************************************************
*/
/* Include ------------------------------------------------------------------*/
#include "my_resource/box_resource.h"
#include <sys/time.h>
#include <stdio.h>

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>

#include <string>
#include <fstream>
#include <iostream>
#include <math.h>
#include <iomanip>
#include <sys/statvfs.h>

// #include "data_structs/data_structs.h"
// #include "configuration/box_config.h"
// #include "share_function/share_function.h"

using namespace BeexBoxResource;
#ifdef INTEL_NUC_BOX
using namespace Poco;
#endif
inline size_t fast_strlen(const char *str)
{
    int i;   // Cannot return 'i' if inside for loop
    for(i = 0; str[i] != '\0'; ++i);

    return i;
}

static inline void mem_init(const char *meminfo[], const char buf[])
{
    const char *var[]  = {
        "MemTotal:",
        "MemFree:",
        "Buffers:",
        "Cached:",
        0
    };

    for	(size_t i = 0; var[i];)
    {
        // TODO: work on making own faster strstr func.
	meminfo[i] = strstr(buf, var[i]) + fast_strlen(var[i]);

        ++i;
    }
}

static inline unsigned long fast_strtoul(const char *c)
{   
    int x = 0;

    // move pointer to first number
    for(; *c < 48 || *c > 57; )
        c++;
   
    // we are on a number, so convert it 
    for(; *c > 47 && *c < 58;)
    {   
        // shifting more efficient than multiplication
        x = (x << 1) + (x << 3) + *c - 48;

        c++;
    }
    
    return x;
}

static inline float mem(int * fd, const char **valp, char * mem_buff)
{
    lseek(*fd, 0, SEEK_SET);
    read(*fd, mem_buff, 256);
    float get_mem = 0;
    get_mem = ((((fast_strtoul(*valp) - fast_strtoul(*(valp + 1) ))
          - (fast_strtoul(*(valp + 2)) + fast_strtoul(*(valp + 3)))) / 1024) / (fast_strtoul(*valp) / 1024.0)) * 100.0; 

    return get_mem;

}

BoxLocation::BoxLocation(){
	this->lat = 0;
	this->lon = 0;
}

BoxLocation::~BoxLocation(){
}


BoxResource::BoxResource(){
}

BoxResource::~BoxResource(){
}

uint8_t BoxResource::getCpuUsage(void){
	std::vector<CPUData> entries1;
	std::vector<CPUData> entries2;

    CpuMonitor cpu;

	// snapshot 1
	cpu.ReadStatsCPU(entries1);

	// 100ms pause
	usleep(100000);

	// snapshot 2
	cpu.ReadStatsCPU(entries2);

	// print output
    const CPUData & e1 = entries1[0];
    const CPUData & e2 = entries2[0];    
    const float ACTIVE_TIME	= static_cast<float>(cpu.GetActiveTime(e2) - cpu.GetActiveTime(e1));
    const float IDLE_TIME	= static_cast<float>(cpu.GetIdleTime(e2) - cpu.GetIdleTime(e1));
    const float TOTAL_TIME	= ACTIVE_TIME + IDLE_TIME;    
    uint8_t value = 100.f * ACTIVE_TIME / TOTAL_TIME;
	//std::cout << "get cpu usage: " << std::to_string(value) << "%" << std::endl;
    return value;
}

uint8_t BoxResource::getRamUsage(void){
    int mem_file          = open("/proc/meminfo", 0);
    const char *meminfo[] = {0, 0, 0, 0, 0};
    char mem_buff[256]; // Just take what's needed
    read(mem_file, mem_buff, 256);
    mem_init(meminfo, mem_buff);

	uint8_t get_mem = mem(&mem_file, meminfo, mem_buff);

    close(mem_file);	

	return get_mem;
}


int BoxResource::getCpuTemp(void){
    std::string val;
    std::string preparedTemp;
    float temperature;

    std::ifstream temperatureFile ("/sys/class/thermal/thermal_zone4/temp");
    std::ifstream temperatureFile02 ("/sys/class/thermal/thermal_zone0/temp");

    if (!temperatureFile.fail()){
        // cout for testing only. return value of -1 should prompt error
        // message.
        // The temperature is stored in 5 digits.  The first two are degrees
        // in C. The rest are decimal precision.
        temperatureFile >> val;
        temperatureFile.close();
        preparedTemp = val.insert(2, 1, '.');
        temperature = std::stod(preparedTemp);
        return (int)(temperature);    
    }
    else if(!temperatureFile02.fail()){
		temperatureFile02 >> val;
		temperatureFile02.close();
		preparedTemp = val.insert(2, 1, '.');
		temperature = std::stod(preparedTemp);
		return (int)(temperature);
    }
    return 0;
}


uint16_t BoxResource::getDiskAvailble(const char *path){
    DiskMonitor disk;
    //return (disk.getAvailableSpace(path) / 1000000000.0);
	return (disk.getAvailableSpace(path));
}


uint16_t BoxResource::getDiskTotal(const char *path){
    DiskMonitor disk;
    //return (disk.getTotalSpace(path) / 1000000000.0);
	return (disk.getTotalSpace(path));
}


bool BoxResource::getNetwork(BoxNetworkInfo *net_info, uint8_t *num){
    FILE *fp = fopen("/proc/net/dev", "r");
    char buf[200], ifname[20];
    unsigned long int r_bytes, t_bytes, r_packets, t_packets;

    // skip first two lines
    for (int i = 0; i < 2; i++) {
        fgets(buf, 200, fp);
    }
    *num = 0;
    uint8_t i = 0;
    while (fgets(buf, 200, fp)) {
        sscanf(buf, "%[^:]: %lu %lu %*lu %*lu %*lu %*lu %*lu %*lu %lu %lu",
               ifname, &r_bytes, &r_packets, &t_bytes, &t_packets);
        //printf("%s: rbytes: %lu rpackets: %lu tbytes: %lu tpackets: %lu\n",
        //       ifname, r_bytes, r_packets, t_bytes, t_packets);
        net_info[i].name = std::string(ifname);
        net_info[i].rx_byte = r_bytes;
        net_info[i].tx_byte = t_bytes;
        (*num)++;
        i++;
    }

    fclose(fp);
}



CpuMonitor::CpuMonitor()
{
}

CpuMonitor::~CpuMonitor()
{
}

void CpuMonitor::ReadStatsCPU(std::vector<CPUData> & entries)
{
	std::ifstream fileStat("/proc/stat");

	std::string line;

	const std::string STR_CPU("cpu");
	const std::size_t LEN_STR_CPU = STR_CPU.size();
	const std::string STR_TOT("tot");

	while(std::getline(fileStat, line))
	{
		// cpu stats line found
		if(!line.compare(0, LEN_STR_CPU, STR_CPU))
		{
			std::istringstream ss(line);

			// store entry
			entries.emplace_back(CPUData());
			CPUData & entry = entries.back();

			// read cpu label
			ss >> entry.cpu;

			// remove "cpu" from the label when it's a processor number
			if(entry.cpu.size() > LEN_STR_CPU)
				entry.cpu.erase(0, LEN_STR_CPU);
			// replace "cpu" with "tot" when it's total values
			else
				entry.cpu = STR_TOT;

			// read times
			for(int i = 0; i < NUM_CPU_STATES; ++i)
				ss >> entry.times[i];
		}
	}
}

size_t CpuMonitor::GetIdleTime(const CPUData & e)
{
	return	e.times[S_IDLE] + 
			e.times[S_IOWAIT];
}

size_t CpuMonitor::GetActiveTime(const CPUData & e)
{
	return	e.times[S_USER] +
			e.times[S_NICE] +
			e.times[S_SYSTEM] +
			e.times[S_IRQ] +
			e.times[S_SOFTIRQ] +
			e.times[S_STEAL] +
			e.times[S_GUEST] +
			e.times[S_GUEST_NICE];
}

void CpuMonitor::PrintStats(const std::vector<CPUData> & entries1, const std::vector<CPUData> & entries2)
{
	const size_t NUM_ENTRIES = entries1.size();

	for(size_t i = 0; i < NUM_ENTRIES; ++i)
	{
		const CPUData & e1 = entries1[i];
		const CPUData & e2 = entries2[i];

		std::cout.width(3);
		std::cout << e1.cpu << "] ";

		const float ACTIVE_TIME	= static_cast<float>(GetActiveTime(e2) - GetActiveTime(e1));
		const float IDLE_TIME	= static_cast<float>(GetIdleTime(e2) - GetIdleTime(e1));
		const float TOTAL_TIME	= ACTIVE_TIME + IDLE_TIME;

		std::cout << "active: ";
		std::cout.setf(std::ios::fixed, std::ios::floatfield);
		std::cout.width(6);
		std::cout.precision(2);
		std::cout << (100.f * ACTIVE_TIME / TOTAL_TIME) << "%";

		std::cout << " - idle: ";
		std::cout.setf(std::ios::fixed, std::ios::floatfield);
		std::cout.width(6);
		std::cout.precision(2);
		std::cout << (100.f * IDLE_TIME / TOTAL_TIME) << "%" << std::endl;
	}
}


DiskMonitor::DiskMonitor(){
}

DiskMonitor::~DiskMonitor(){
}

long DiskMonitor::getAvailableSpace(const char* path){
    struct statvfs stat;

    if (statvfs(path, &stat) != 0) {
    // error happens, just quits here
    return -1;
    }

    // the available size is f_bsize * f_bavail
    return (stat.f_bsize * stat.f_bavail) / 1000000000;
}

long DiskMonitor::getTotalSpace(const char *path){
    struct statvfs stat;

    if (statvfs(path, &stat) != 0) {
    // error happens, just quits here
    return -1;
    }

    // the available size is f_bsize * f_bavail
    return (stat.f_bsize * stat.f_blocks) / 1000000000;    
}