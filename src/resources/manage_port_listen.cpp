/**
******************************************************************************
* @file           : manage_port_listen.cpp
* @brief          : source file of manage port listen
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#include "resources/manage_port_listen.h"
#include "logger/webe_logger.h"
#include <fcntl.h>
#include <netinet/in.h>
#include <netdb.h>
#include <iostream>
#include "common/common_data.h"

using namespace BeexResourceManage;

ManagePortListen workerManagePortListen;


bool ManagePortListen::checkPortInUse(uint32_t port){
	bool result = false;
	struct sockaddr_in serveraddr;
    int new_socket;

    new_socket = socket(AF_INET, SOCK_STREAM, 0);

	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = INADDR_ANY;
	serveraddr.sin_port = htons(port);
	if (bind(new_socket, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) < 0)
	{
		std::cout << "port busy" << port << std::endl;
		close(new_socket);
		result = true;
	}
	else
	{
		// std::cout << "port available" << std::endl;
		close(new_socket);
		result = false;
	}
	return result;
}


uint32_t ManagePortListen::getPortAvailable(void){
	uint32_t portResult = 0;
	while (1){
		if(this->checkPortInUse(this->currentPort)){
			this->currentPort++;
			if(this->currentPort > this->END_PORT){
				this->currentPort = START_PORT;
			}
		}
		else{
			// port available
			// check dulicate
			if(!listRtspChannel.findPortChannel(this->currentPort)){
				portResult = this->currentPort;
				this->currentPort++;
				break;
			}
			else{
				this->currentPort++;
			}
		}
		// usleep(1000);
	}
	return portResult;
}
