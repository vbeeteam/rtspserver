#include <iostream>
#include "rtspserver/rtsp_server.h"
#include "common/common_data.h"
#include <thread>
#include <unistd.h>
#include "resources/manage_port_listen.h"
#include "api_service/worker_httpserver.h"
#include "config/config.hpp"
#include "logger/webe_logger.h"
#include "api_service/api_service_worker.hpp"
#include "monitor/worker_manage_ping.h"

using namespace BeexCommonData;
using namespace BeexRtspServer;
using namespace BeexResourceManage;
using namespace BeexApiService;
using namespace CommonEngine;
using namespace ApiService;
using namespace ManagePing;

BeexApiService::WorkerHttpServer workerHttpServer;

int main(int argc, char** argv){
    if(argc < 2){
        std::cout << "enter a application path" << std::endl;
        exit(1);
    }
    std::string appPath(argv[1]);

    // init and load configuration
    commonConfig.init(appPath);
    config_info common_info;

    // init logger
    spdLogger.init(appPath);

    listRtspChannel.updatePath(appPath);
    // load data
    listRtspChannel.init();

    // test ping
    // std::thread WorkerManagePingThread(&WorkerManagePing::run, &workerManagePing);
    std::thread WorkerApiServiceThread(&ApiServiceWorker::run, &apiServiceWorker);

    WorkerApiServiceThread.join();
    // WorkerManagePingThread.join();

    exit(1);

    // std::thread WorkerHttpServerThread(&WorkerHttpServer::run, &workerHttpServer);
    // std::thread WorkerRtspServerThread(&RtspServer::run, &workerRtspServer);

    // WorkerHttpServerThread.join();

    while (1)
    {
        sleep(10);
        for (uint8_t i = 0; i < 100; i++)
        {
            RtspChannelInfo channelInfo;
            channelInfo.cameraSerial = std::to_string(i);
            channelInfo.serviceId = "1";
            channelInfo.port = 1000 + i;
            channelInfo.isActive = true;

            bool state = false;
            listRtspChannel.updateActiveState(channelInfo, state);
            
            usleep(1000);
        } 
            while (1)
            {
                usleep(1000);
            }               
    }
    
}