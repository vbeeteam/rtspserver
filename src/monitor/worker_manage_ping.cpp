/**
******************************************************************************
* @file           : worker_manage_ping.cpp
* @brief          : source file of worker manage Ping
******************************************************************************
******************************************************************************
*/
// #define CPPHTTPLIB_OPENSSL_SUPPORT
#include "http_lib/httplib.h"


#include <unistd.h>
#include <iostream>
#include <thread>
#include "monitor/worker_manage_ping.h"
// #include "os_function/os_func.h"
#include "rapidjson/document.h"			
#include "rapidjson/writer.h"
#include "my_resource/box_resource.h"
#include "api_service/api_httpclient.h"
#include "utility/utility.hpp"
#include "logger/webe_logger.h"
// #include "configuration/box_config.h"




using namespace ManagePing;
// using namespace VBeeDataStructs;
using namespace rapidjson;
using namespace BeexApiService;
using namespace CommonEngine;

WorkerManagePing workerManagePing;

WorkerManagePing::WorkerManagePing() {
	
}

WorkerManagePing::~WorkerManagePing() {
}

/*
    "cpu":30,
    "ram":40,
    "disk":23,
    "totalDisk":50,
    "temperature":56,

	*/

void WorkerManagePing::run(void) {
	std::cout << "WorkerManagePing::run - start"  << std::endl;

	std::thread GetResourceThread(&WorkerManagePing::getResource, this);
	std::thread ReportGwInfoThread(&WorkerManagePing::handlePing, this);

	GetResourceThread.join();
	ReportGwInfoThread.join();
}


void WorkerManagePing::getResource(void){
	std::cout << "WorkerManagePing::getResource - start"  << std::endl;
	while (1){
		// gateway info
		BeexBoxResource::BoxResource resource;
		uint8_t cpu = resource.getCpuUsage();
		uint8_t ram = resource.getRamUsage();
		uint16_t total_disk = resource.getDiskTotal("/");
		uint16_t disk = total_disk - resource.getDiskAvailble("/");
		int temp = resource.getCpuTemp();
		uint32_t network_in = 0;
		uint32_t network_out = 0;
		std::string localIp = "localhost";
		std::string publicIp = "103.63.108.20";
		uint8_t janus_status = 0;
		uint32_t firmwareVersion = 1;

		BeexBoxResource::BoxNetworkInfo net_info[5];
		uint8_t card_num = 0;
		uint32_t net_rx = 0, net_tx = 0, net_rx_pre = 0, net_tx_pre = 0;
		for(uint8_t i = 0; i<2; i++){
			resource.getNetwork(net_info, &card_num);
			net_rx_pre = net_rx;
			net_tx_pre = net_tx;					
			net_rx = 0;
			net_tx = 0;
			for(uint8_t i=0; i<card_num; i++){
				net_rx += net_info[i].rx_byte;
				net_tx += net_info[i].tx_byte;
			}
			//std::cout << "get network info: netIn = " << std::to_string(net_in-net_in_pre) << " - netOut = " << std::to_string(net_out-net_out_pre) << std::endl;
			if(i == 1){
				break;
			}
			sleep(1);
		}
		network_in = (net_rx - net_rx_pre) / 1000;
		network_out = (net_tx - net_tx_pre) / 1000;

		// update
		this->reportInfo.cpu = cpu;
		this->reportInfo.ram = ram;
		this->reportInfo.disk = disk;
		this->reportInfo.totalDisk = total_disk;
		this->reportInfo.temperature = temp;
		this->reportInfo.networkIn = network_in;
		this->reportInfo.networkOut = network_out;
		this->reportInfo.janus = janus_status;
		this->reportInfo.extra = "NULL";
		this->reportInfo.localIp = localIp;
		this->reportInfo.publicIp = publicIp;
		sleep(10);
	}
	
}


void WorkerManagePing::handlePing(void){
	std::cout << "WorkerManagePing::handlePing - start"  << std::endl;
	while (1){
		try
		{		
			std::stringstream out;
			StringBuffer jsonBuffer;
			Writer<StringBuffer> writer(jsonBuffer);
			std::string jsonOut;

			writer.StartObject();
			writer.Key("cpu");
			writer.Uint(this->reportInfo.cpu);
			writer.Key("memory");
			writer.Uint(this->reportInfo.ram);
			writer.Key("freeDisk");
			writer.String(std::to_string(this->reportInfo.disk).c_str());
			writer.Key("totalDisk");
			writer.String(std::to_string(this->reportInfo.totalDisk).c_str());
			writer.Key("host");
			writer.String(this->reportInfo.publicIp.c_str());
			writer.Key("application");
			writer.String("open_streaming");
			writer.Key("localIp");
			writer.String(this->reportInfo.localIp.c_str());
			writer.Key("infoAttachment");
			writer.String("{\"ipAddress\":\"103.63.108.20\",\"portApp\":8000,\"portThrift\":9000}");
			writer.EndObject();

			jsonOut = jsonBuffer.GetString();

			std::string api_url = "https://resource-api.beex.vn:8050/resourceClouds/ping";
			std::string token = "Basic NS01BhqBYSksuvhqJO0b16O86C0wPwkM";
			std::string respone;
			
			// parse url
			fields_url_info url_info;
			if(UtilityFunc::parseUrl(api_url, url_info) == false){
				spdLogger.common->info("ApiServiceWorker - reqRtspChannel: parseUrl error = {}", api_url);
				continue;
			}

			std::string body = "";
			if(url_info.scheme == "https"){
				// note cacert file
				ApiHttpClient http_client;
				http_client.requestPostByHttps(api_url, token, jsonOut, &respone);
				// httplib::Client cli(url_info.host, url_info.port);
				// cli.set_ca_cert_path ( "./ca-bundle.crt" );
				// cli.enable_server_certificate_verification (false);    
				// use header
				// httplib::Headers headers = {
				// 	{ "Authorization", token }
				// };
				// auto res = cli.Post(url_info.path.c_str(), headers, jsonOut.c_str(), "application/json");
				// auto err = res.error();
				// std::cout << res << err << std::endl;
				// if(err == httplib::Error::Success) {
				// 	if(res->status == 200){
				// 		body = res->body;
				// 	}
				// }
			}
			else if(url_info.scheme == "http"){
				httplib::Client cli(url_info.host, url_info.port);
				httplib::Headers headers = {
					{ "Authorization", token }
				};
				auto res = cli.Post(url_info.path.c_str(), headers, jsonOut.c_str(), "application/json");
				auto err = res.error();    
				if(err == httplib::Error::Success) {
					if(res->status == 200){
						body = res->body;
					}
				}    
			}

			std::cout << "WorkerManagePing::handlePing - " << jsonOut << " - response = " << respone << std::endl;			
		}
		catch(const std::exception& e){
			std::cout << "WorkerManagePing::handlePing - exception - " << e.what() << std::endl;
		}		
		sleep(60);
	}
}