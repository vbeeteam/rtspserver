/**
******************************************************************************
* @file           : config.hpp
* @brief          : Header file of configuration
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef CONFIG_H_
#define CONFIG_H_

/* Include ------------------------------------------------------------------*/
#include <string>
#include <unistd.h>

namespace CommonEngine{
    typedef struct{
        std::string public_ip;
        uint64_t api_service_port;
        uint16_t rtsp_monitor_stream_port;
        std::string rtsp_username;
        std::string rtsp_password;
    } config_info;

    class Config{
    private:
        // attributes
        config_info configInfo;
        std::string app_path;
        std::string file_path;

        // methods
        bool load(void);
    public:
        Config(){
            this->app_path = "/tmp";
            this->file_path = this->app_path + "/config/common.txt";

            this->configInfo.public_ip = "127.0.0.1";
            this->configInfo.api_service_port = 0;
            this->configInfo.rtsp_monitor_stream_port = 8554;
            this->configInfo.rtsp_username = "admin";
            this->configInfo.rtsp_password = "admin123";
        }
        ~Config(){
            
        }
        //attributes

        //method
        void init(const std::string &path){
            this->app_path = path;
            this->file_path = this->app_path + "/config/common.txt";
            std::string cmd = "mkdir -p " + this->app_path + "/config";
            system(cmd.c_str());
            this->load();
        }
        void getConfig(config_info &cfgInfo){
            cfgInfo = this->configInfo;
        }
    };
}

extern CommonEngine::Config commonConfig;

#endif //