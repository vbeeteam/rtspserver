/**
******************************************************************************
* @file           : config.cpp
* @brief          : Source file of configuration
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
/* Include ------------------------------------------------------------------*/
#include "config/config.hpp"
#include "utility/utility.hpp"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include <iostream>

using namespace CommonEngine;
using namespace rapidjson;

Config commonConfig;

bool Config::load(void){
    std::cout << "Config::loadConfig - start" << std::endl;

    bool result = false;
    // load file config
    if(UtilityFunc::checkFile(this->file_path)){
        FILE *file = NULL;
        size_t len;
        ssize_t read;
        char *line = NULL;
        file = fopen(this->file_path.c_str(), "r");
        if(file != NULL){
            read = getline(&line, &len, file);
            if(read != -1){
                std::string json_data(line);
                std::cout << "Config::loadConfig - read config file = " << json_data << std::endl;

                try{
                    // parse json data        
                    Document document;
                    document.Parse(json_data.c_str());
                    if(document.HasMember("publicIp")){
                        std::string publicIp = document["publicIp"].GetString();
                        this->configInfo.public_ip = publicIp;
                    }
                    if(document.HasMember("apiServicePort")){
                        int apiServicePort = document["apiServicePort"].GetInt();
                        this->configInfo.api_service_port = apiServicePort;
                    }
                    if(document.HasMember("rtspMonitorStreamPort")){
                        int rtspMonitorStreamPort = document["rtspMonitorStreamPort"].GetInt();
                        this->configInfo.rtsp_monitor_stream_port = rtspMonitorStreamPort;
                    }
                    if(document.HasMember("rtspUsername")){
                        std::string rtspUsername = document["rtspUsername"].GetString();
                        this->configInfo.rtsp_username = rtspUsername;
                    }
                    if(document.HasMember("rtspPassword")){
                        std::string rtspPassword = document["rtspPassword"].GetString();
                        this->configInfo.rtsp_password = rtspPassword;
                    }
                    result = true;
                }
                catch(const std::exception& e){
                    std::cout << "Config::loadConfig - exception when parse json data = " << json_data << std::endl;
                    result = false;
                }
            }
            else{
                std::cout << "Config::loadConfig - config file not read" << std::endl;
            }
        }
        else{
            std::cout << "Config::loadConfig - config file not opened" << std::endl;
        }
        fclose(file);
    }
    else{
        std::cout << "Config::loadConfig - config file not found" << std::endl;
        UtilityFunc::creatFile(this->file_path);
    }
    return result;
}