/**
******************************************************************************
* @file           : utility.hpp
* @brief          : header file of utility function
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef UTILITY_FUNCTION_H
#define UTILITY_FUNCTION_H

#include <stdint.h>
#include <stdbool.h>
#include <string>
#include <fstream>
#include <iostream>
#include <math.h>
#include <iomanip>

namespace CommonEngine{

    typedef struct _fields_url_info{
        std::string scheme;
        std::string host;
        int port;
        std::string path;
    } fields_url_info;

    class UtilityFunc{
    private:
        
    public:
        UtilityFunc(){

        }
        ~UtilityFunc(){

        }

        static uint32_t getTimestamp(void);
        static uint32_t getTimeMs(void);
        static uint8_t splitString(std::string data, std::string key_split, std::string *data_split);
        static bool checkFile(std::string path);
        static std::string fileName(std::string path);
        static bool creatFile(std::string path);
        static uint16_t getCurrentAsMinuteInDay(void);
        static bool parseUrl(const std::string &url, fields_url_info &urlInfo);
    };
    

}


#endif // !UTILITY_FUNCTION_H
