/**
******************************************************************************
* @file           : utility.cpp
* @brief          : Source file of utility function
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#include "utility/utility.hpp"
#include <sys/time.h>
#include <stdio.h>

#include <unistd.h> 
#include <string.h> 
#include <stdio.h>  
#include <fcntl.h> 
#include <sys/stat.h>
#include <time.h>
#include <curl/curl.h>


using namespace CommonEngine;

uint32_t UtilityFunc::getTimestamp(void){
	struct timeval tv;
	unsigned long get_time = 0;
	gettimeofday(&tv, NULL);
	
	get_time = tv.tv_sec;
	return get_time;
}


uint32_t UtilityFunc::getTimeMs(void){
	struct timeval tv;
	uint32_t get_time = 0;
	gettimeofday(&tv, NULL);
	
	get_time = tv.tv_usec;
	return get_time / 1000;
}

uint8_t UtilityFunc::splitString(std::string data, std::string key_split, std::string *data_split){
    uint16_t pos = 0;
    uint16_t i = 0;

    
    while ((pos = data.find(key_split)) != std::string::npos) {
        data_split[i] = data.substr(0, pos);
        data.erase(0, pos + 1);
        i++;
        if (i > 9) {
            return 9;
        }
    }
    return i;
}

bool UtilityFunc::checkFile(std::string path){
    struct stat buffer;   
    return (stat (path.c_str(), &buffer) == 0); 
}


std::string UtilityFunc::fileName(std::string path){
  size_t found = path.find_last_of("/\\");
  std::string name = path.substr(found+1); // check that is OK
  return name;
}

bool UtilityFunc::creatFile(std::string path){
    std::ofstream outfile (path, std::fstream::app);
    outfile.close();
}

uint16_t UtilityFunc::getCurrentAsMinuteInDay(void){
    time_t rawTime;
    struct tm * timeInfo;
    time (&rawTime);
    timeInfo = localtime (&rawTime);
    uint8_t hour = timeInfo->tm_hour;
    uint8_t minute = timeInfo->tm_min;
    uint32_t currentMinute = hour * 60 + minute;
    return currentMinute;
}


bool UtilityFunc::parseUrl(const std::string &url, fields_url_info &urlInfo){
    bool result = false;

    CURLU *h;
    CURLUcode uc;
    char *host;
    char *path;    
    char *port;
    char *scheme;
    h = curl_url(); /* get a handle to work with */
    
    /* parse a full URL */
    uc = curl_url_set(h, CURLUPART_URL, url.c_str(), 0);
    if(!uc){
        uc = curl_url_get(h, CURLUPART_HOST, &host, 0);
        if(!uc) {
            result = true;
            // printf("Host name: %s\n", host);
            urlInfo.host = std::string(host);
            curl_free(host);
        }
        uc = curl_url_get(h, CURLUPART_PATH, &path, 0);
        if(!uc) {
            // printf("Path: %s\n", path);
            urlInfo.path = std::string(path);
            curl_free(path);
        }
        uc = curl_url_get(h, CURLUPART_PORT, &port, 0);
        if(!uc) {
            // printf("Port: %s\n", port);
            std::string port_cvt = std::string(host);
            urlInfo.port = std::stoi(port_cvt);
            curl_free(port);
        }
        uc = curl_url_get(h, CURLUPART_SCHEME, &scheme, 0);
        if(!uc) {
            // printf("scheme: %s\n", scheme);
            urlInfo.scheme = std::string(scheme);
            curl_free(scheme);
        }         
    }
    curl_url_cleanup(h);

    return result;
}