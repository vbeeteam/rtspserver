# dependence
sudo apt-get install -y g++ make cmake openssl libssl-dev libcurl4-nss-dev

# lib open source
cd /tmp/
# poco
git clone -b master https://github.com/pocoproject/poco.git
cd poco
mkdir cmake-build && cd cmake-build
cmake --config Release ..
make -j4
sudo mkdir /opt/poco
sudo make install DESTDIR=/opt/poco
sudo mv /opt/poco/usr/local/* /opt/poco/
echo "/opt/poco/lib/" > /etc/ld.so.conf.d/x86_64-linux-gnu.conf
# source code
cd /tmp/
git clone https://tuannguyen101@bitbucket.org/vbeeteam/rtspserver.git
cd rtspserver
rm -r build
mkdir build && cd build
cmake ..
make -j4
../script/deploy.sh