sudo systemctl stop stream_monitor.service
sudo systemctl stop rtsp_simple_server.service

sudo mkdir -p /opt/rtsp_server
sudo mkdir -p /opt/rtsp_server/rtsp-simple-server
sudo mkdir -p /opt/rtsp_server/config/

sudo cp ./streamMonitor /opt/rtsp_server
sudo cp ../rtsp-simple-server-install/* /opt/rtsp_server/rtsp-simple-server

sudo cp ../cacert/cacert.pem /opt/rtsp_server/
# sudo cp ../config/* /opt/rtsp_server/config/

sudo cp ../script/stream_monitor.service /etc/systemd/system/
sudo cp ../script/rtsp_simple_server.service /etc/systemd/system/

sudo systemctl daemon-reload
sudo systemctl enable stream_monitor.service
sudo systemctl enable rtsp_simple_server.service

sudo systemctl restart rtsp_simple_server.service
sudo systemctl restart stream_monitor.service