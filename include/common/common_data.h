/**
******************************************************************************
* @file           : common_data.h
* @brief          : header file of common data
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef COMMON_DATA_H
#define COMMON_DATA_H

#include <string>
#include <stdint.h>
#include <stdbool.h>
#include <map>
#include <mutex>
#include <algorithm>

#define BEEX_PATH  "/opt/beexApp/rtsp_server/"
namespace BeexCommonData{

    class MonitorStreamInfo{
    private:
        
    public:
        MonitorStreamInfo(){
            this->gateway_serial = "None";
        }
        ~MonitorStreamInfo(){

        }
        // attributes
        std::string serial;
        std::string name_space;
        uint16_t service_id;
        uint16_t monitor_stream_id;
        std::string stream_path;
        std::string host;
        uint64_t port;
        std::string username;
        std::string password;
        std::string scheme;
        std::string gateway_serial;
    };
    

    class RtspChannelInfo{
    public:
        RtspChannelInfo(){
            this->isActive = false;
            this->isRunning = false;
            this->isNewChannel = false;
        }
        ~RtspChannelInfo(){

        }
        // attribute
        std::string cameraSerial;
        std::string serviceId;
        uint32_t subprocessId;
        uint32_t port;
        std::string host;
        std::string pubUsername;
        std::string pubPassword;
        std::string rtspStream;
        std::string name_space;

        bool isActive;
        bool isRunning;
        bool isNewChannel;
    };

    class RtspChannelHandle{
    private:
        std::map<std::string, RtspChannelInfo> listChannel;
        std::mutex locker;
        std::string app_path;
        std::string file_path;

        bool load(void);
        bool save(void);
      
    public:
        RtspChannelHandle(){
            this->app_path = "/tmp";
            this->file_path = this->app_path + "/data/data.txt";
        }
        ~RtspChannelHandle(){

        }
        bool init(void){
            this->load();
        }
        bool addChannel(RtspChannelInfo &channelInfo);
        bool findChannel(RtspChannelInfo &channelInfo);
        bool getChannel(std::string channelKey, RtspChannelInfo *channelInfo);
        bool getNewChannel(RtspChannelInfo *channelInfo);
        bool eraseChannel(RtspChannelInfo &channelInfo);
        bool findPortChannel(uint32_t port);

        bool updateActiveState(RtspChannelInfo &channelInfo, bool &state);
        bool updateRunState(RtspChannelInfo &channelInfo, bool &state);
        bool updateSubProcessId(RtspChannelInfo &channelInfo, uint32_t &p_id);
        bool clearNewChannelState(RtspChannelInfo &channelInfo);
        void updatePath(const std::string &path){
            this->app_path = path;
            this->file_path = this->app_path + "/data/data.txt";
        }  
    };

    class MonitorStreamChannel{
    private:
        std::map<std::string, MonitorStreamInfo> listChannel;
        std::mutex locker;
        std::string app_path;
        std::string file_path;

        bool load(void);
        bool save(void);
      
    public:
        MonitorStreamChannel(){
            this->app_path = "/tmp";
            this->file_path = this->app_path + "/data/monitor_stream.txt";
            std::string cmd = "mkdir -p " + this->app_path + "/data";
            system(cmd.c_str());            
        }
        ~MonitorStreamChannel(){

        }
        bool init(const std::string &path){
            this->app_path = path;
            this->file_path = this->app_path + "/data/data.txt";
            this->load();
        }
        bool createKey(const MonitorStreamInfo &streamInfo, std::string &key){
            std::string my_str = streamInfo.name_space;
            my_str.erase(std::remove(my_str.begin(), my_str.end(), '.'), my_str.end());
            key = my_str + "-" + streamInfo.gateway_serial + "-" + streamInfo.serial + "-" + std::to_string(streamInfo.service_id) + "-" + std::to_string(streamInfo.monitor_stream_id);
        }
        bool addChannel(const MonitorStreamInfo &channelInfo);
        bool findChannel(const MonitorStreamInfo &channelInfo);
        bool getChannel(std::string channelKey, MonitorStreamInfo &channelInfo);
        bool eraseChannel(const MonitorStreamInfo &channelInfo);
    };

    
    
} // namespace BeexCommonData

extern BeexCommonData::RtspChannelHandle listRtspChannel;
extern BeexCommonData::MonitorStreamChannel listMonitorStream;

#endif
