/**
******************************************************************************
* @file           : box_resource.h
* @brief          : Header file of resource in box
******************************************************************************
******************************************************************************
*/
#ifndef BOX_RESOURCE_H
#define BOX_RESOURCE_H
/* Include ------------------------------------------------------------------*/
#include "stdint.h"
#include <chrono>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <thread>
#include <vector>


namespace BeexBoxResource{
    const int NUM_CPU_STATES = 10;

    enum CPUStates
    {
        S_USER = 0,
        S_NICE,
        S_SYSTEM,
        S_IDLE,
        S_IOWAIT,
        S_IRQ,
        S_SOFTIRQ,
        S_STEAL,
        S_GUEST,
        S_GUEST_NICE
    };    


    typedef struct CPUData
    {
        std::string cpu;
        size_t times[NUM_CPU_STATES];
    } CPUData;


    class CpuMonitor
    {
    private:
        
    public:
        CpuMonitor();
        ~CpuMonitor();

        void ReadStatsCPU(std::vector<CPUData> & entries);

        size_t GetIdleTime(const CPUData & e);
        size_t GetActiveTime(const CPUData & e);
        void PrintStats(const std::vector<CPUData> & entries1, const std::vector<CPUData> & entries2);

    };

    class DiskMonitor
    {
    private:
        
    public:
        DiskMonitor();
        ~DiskMonitor();

        long getAvailableSpace(const char *path);
        long getTotalSpace(const char *path);
    };
    
    class BoxLocation{
    private:
        
    public:
        BoxLocation();
        ~BoxLocation();

        double lat;
        double lon;
    };
    

    class BoxNetworkInfo
    {
    private:
        
    public:
        std::string name;
        uint32_t tx_byte;
        uint32_t rx_byte;
    };

    class BoxResource{
    private:
        bool requestHttp(std::string api, std::string *msg);
        bool requestHttps(std::string api, std::string *msg);
        
        BoxLocation getLocationByIp(void);        
    public:
        BoxResource();
        ~BoxResource();

        // Attribute
        bool getMyPublicIP(std::string *ip);
        BoxLocation getLocation(void);

        uint8_t getCpuUsage(void);
        uint8_t getRamUsage(void);
        int getCpuTemp(void);
        uint16_t getDiskAvailble(const char *path);
        uint16_t getDiskTotal(const char *path);    
        bool getNetwork(BoxNetworkInfo *net_info, uint8_t *num); 
        bool checkJanusEngine(std::string localIp);
        void restartJanusEngine(void);
        bool getFirmwareVersion(uint32_t *version);

        void speedTestNet(void);
    };
    
}


#endif