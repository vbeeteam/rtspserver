/**
******************************************************************************
* @file           : worker_manage_ping.h
* @brief          : header file of worker manage Ping
******************************************************************************
******************************************************************************
*/
#ifndef WORKER_MANAGE_PING_H
#define WORKER_MANAGE_PING_H

#include <stdint.h>
#include <string>

namespace ManagePing
{
	class ReportInfo{
	private:
		
	public:
		ReportInfo(){
			this->firmwareVersion = 1;
		}
		~ReportInfo(){
			
		}

		int cpu;
		int ram;
		int disk;
		int totalDisk;
		int temperature;
		int networkIn;
		int networkOut;
		int janus;
		std::string extra;
		std::string localIp;
		std::string publicIp;
		uint32_t firmwareVersion;
		std::string firmwareOs;
		std::string featureEngineModel;
	};

	
	class WorkerManagePing
	{
	public:
		WorkerManagePing();
		~WorkerManagePing();
		
		void run(void);
		void getResource(void);
		void handlePing(void);

	private:
		static const uint32_t timeout_ping = 11; // seconds
		ReportInfo reportInfo;
	};

}

extern ManagePing::WorkerManagePing workerManagePing;

#endif // !WORKER_MANAGE_PING_H
