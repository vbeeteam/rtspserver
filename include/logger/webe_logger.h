/**
******************************************************************************
* @file           : webe_logger.h
* @brief          : header file of Webe logger
******************************************************************************
******************************************************************************
*/
#ifndef WEBE_LOGGER_H
#define WEBE_LOGGER_H

/* Include ------------------------------------------------------------------*/
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"
#include <string>

namespace WebeLogger{
    class Logger{
    private:
        std::string path;
        std::string name;
        
        // methods
        
    public:
        Logger(){

        }
        ~Logger(){

        }
        std::shared_ptr<spdlog::logger>  common;
        
        void init(const std::string &app_path);        
    };
}


/* object and variable Public ---------------------------------------------------------*/
extern WebeLogger::Logger spdLogger;

#endif
