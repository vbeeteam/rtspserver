/**
******************************************************************************
* @file           : rtsp_server.h
* @brief          : header file of rtsp server
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef RTSP_SERVER_H
#define RTSP_SERVER_H

#include <stdbool.h>
#include <stdint.h>
#include "common/common_data.h"

namespace BeexRtspServer{
    class RtspServer{
    private:
        // Attribute
        static const uint16_t CHANNEL_NUM_MAX = 100;
        uint16_t channelNum;

        // method
        void startChannel(BeexCommonData::RtspChannelInfo channelInfo);
        bool stopChannel(BeexCommonData::RtspChannelInfo channelInfo);
        
    public:
        RtspServer(){
            this->channelNum = 0;
        }
        ~RtspServer(){

        }
        void run(void);
        void handleChannel(BeexCommonData::RtspChannelInfo chanelInfo);
    };
    
    
} // namespace BeexRtspServer

extern BeexRtspServer::RtspServer workerRtspServer;

#endif
