/**
******************************************************************************
* @file           : manage_port_listen.h
* @brief          : header file of manage port listen
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef MANAGE_PORT_LISTEN_H
#define MANAGE_PORT_LISTEN_H

#include <stdint.h>

namespace BeexResourceManage{
    class ManagePortListen{
    private:
        uint32_t currentPort;
        static const uint32_t START_PORT = 40001;
        static const uint32_t END_PORT = 40101;

        bool checkPortInUse(uint32_t port);
        
        
    public:
        ManagePortListen(){
            this->currentPort = START_PORT;
        }
        ~ManagePortListen(){

        }

        uint32_t getPortAvailable(void);
    };
    
} // namespace BeexResourceManage


extern BeexResourceManage::ManagePortListen workerManagePortListen;

#endif
