/**
******************************************************************************
* @file           : api_service_worker.hpp
* @brief          : Header file of Worker for api service
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef API_SERVICE_WORKER_H
#define API_SERVICE_WORKER_H

/* Include ------------------------------------------------------------------*/
#include <string>
#include "http_lib/httplib.h"

namespace ApiService{

    class ApiServiceWorker{
    private:
        // attributes
        httplib::Server svr;
        std::string ip_addr;
        std::string ip_public;
        int port;

        // methods
        void init(void);
        void addMonitorStreamChannel(void);
    public:
        ApiServiceWorker(){
            this->ip_addr = "0.0.0.0";
            this->port = 9008;
        }
        ~ApiServiceWorker(){

        }
        // attributes


        // methods
        void run(void);
    };
    

} // namespace ApiService


extern ApiService::ApiServiceWorker apiServiceWorker;

#endif
