/**
******************************************************************************
* @file           : worker_httpserver.h
* @brief          : Header file of Worker for http server
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef WORKER_HTTPSERVER_H
#define WORKER_HTTPSERVER_H 

#define USER_HTTPSERVER_PORT            9009

namespace BeexApiService{
    class WorkerHttpServer{
    private:
        
    public:
        WorkerHttpServer(){

        }
        ~WorkerHttpServer(){
            
        }

        void run(void);
    };
}



#endif
