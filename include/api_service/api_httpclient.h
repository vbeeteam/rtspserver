/**
******************************************************************************
* @file           : api_httpclient.h
* @brief          : Header file of API HttpClient
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef API_HTTPCLIENT_H
#define API_HTTPCLIENT_H

#include "Poco/Net/SSLManager.h"
#include <string>

namespace BeexApiService{
    class SSLInitializer {
    public:
        SSLInitializer() { Poco::Net::initializeSSL(); }

        ~SSLInitializer() { Poco::Net::uninitializeSSL(); }
    };

    class ApiHttpClient{
    private:
        std::string cacertPath;

        bool requestPostByHttp(std::string &api, std::string &data, std::string *msgResp);
        bool requestPostByHttps(std::string &api, std::string &data, std::string *msgResp);
        bool requestDeleteByHttp(std::string &api, std::string &data, std::string *msgResp);
        bool requestDeleteByHttps(std::string &api, std::string &data, std::string *msgResp);

    public:
        ApiHttpClient(){
            this->cacertPath = "/opt/rtsp_server/";

        }
        ~ApiHttpClient(){

        }
        bool requestPostByHttps(std::string &api, std::string &token, std::string &data, std::string *msgResp);
        bool addRtspChannelStream(std::string &cameraSerial, std::string &serviceId, std::string &streamUrl, std::string &name_space);
        bool deleteRtspChannelStream(std::string &cameraSerial, std::string &serviceId);
    };
    
} // namespace BeexApiService


#endif
